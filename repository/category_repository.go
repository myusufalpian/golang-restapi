package repository

import "golang-restapi/entity"

type CategoryRepository interface {
	FindById(id int) *entity.Category
}
