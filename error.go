package main

import (
	"errors"
	"fmt"
)

var (
	NotFoundError   = errors.New("Data not found")
	ValidationError = errors.New("Data validation error")
	BadRequestError = errors.New("Bad request")
)

func Divide(value int, divisor int) (int, error) {
	if divisor == 0 {
		return 0, errors.New("Divisor cannot be zero")
	}
	return value / divisor, nil
}

func getById(id string) error {
	if id == "" {
		return ValidationError
	} else if id != "yusuf" {
		return NotFoundError
	}
	return nil
}

func main() {
	result, err := Divide(10, 1)
	if err != nil {
		fmt.Println(err.Error())
	} else {
		fmt.Println("Result =", result)
	}

	getId := getById("yusuf")
	if getId != nil {
		fmt.Println(getId.Error())
	} else {
		fmt.Printf("Hello, %s", "yusuf")
	}
}
