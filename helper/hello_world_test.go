package helper

import (
	"fmt"
	"github.com/stretchr/testify/assert"
	"github.com/stretchr/testify/require"
	"runtime"
	"testing"
)

func TestHelloWorld(t *testing.T) {
	result := HelloWorld("Yusuf")
	assert.Equal(t, "Hello Yusuf", result)
	fmt.Println("Result =", result)
}

func BenchmarkHelloWorld(b *testing.B) {
	for i := 0; i < b.N; i++ {
		HelloWorld("Yusuf")
	}
}

func TestSkip(t *testing.T) {
	if runtime.GOOS == "darwin" {
		t.Skip("Unit test tidak bisa jalan di MacOS")
	} else {
		t.Log("Unit test tidak bisa jalan di Windows dan Linux")
	}
}

func TestMain(m *testing.M) {
	fmt.Println("Sebelum Test Main")
	m.Run()
	fmt.Println("Setelah Test Main")
}

func TestSubTest(t *testing.T) {
	t.Run("Yusuf", func(t *testing.T) {
		result := HelloWorld("Yusuf")
		require.Equal(t, "Hello Yusuf", result)
	})

	t.Run("Alpian", func(t *testing.T) {
		result := HelloWorld("Alpian")
		require.Equal(t, "Hello Alpian", result)
	})
}

func TestTableTest(t *testing.T) {
	tests := []struct {
		name     string
		request  string
		expected string
	}{
		{
			name:     "HelloWorld(Muhammad)",
			request:  "Muhammad",
			expected: "Hello Muhammad",
		},
		{
			name:     "HelloWorld(Yusuf)",
			request:  "Yusuf",
			expected: "Hello Yusuf",
		},
		{
			name:     "HelloWorld(Alpian)",
			request:  "Alpian",
			expected: "Hello Alpian",
		},
	}
	for _, test := range tests {
		t.Run(test.name, func(t *testing.T) {
			result := HelloWorld(test.request)
			require.Equal(t, test.expected, result)
		})
	}
}
